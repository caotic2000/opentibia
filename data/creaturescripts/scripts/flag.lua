function onStatsChange(cid, attacker, type, combat, value)

if type == 1 and isPlayer(cid) and FLAG.PLAYER.isTeam(cid) and FLAG.PLAYER.getCaptureFlag(cid) and getCreatureHealth(cid) - value <= 0 then
local pos = getCreaturePosition(cid)
doCleanTile(getCreaturePosition(cid))
addEvent(doSendAnimatedText, 1000, pos, "Pegue a flag", 40)
FLAG.PLAYER.doRetireFlagPlayer(cid)
return false
end

if type == 1 and isPlayer(cid) and FLAG.PLAYER.isTeam(cid) and getCreatureHealth(cid) - value <= 0 then
FLAG.PLAYER.doBackPlayerToBase(cid)
return false
end

return true
end

function onLogin(cid)

if FLAG.PLAYER.isTeam(cid) then
FLAG.PLAYER.doExit(cid)
return true
end

return true
end


function onLogout(cid)

if FLAG.PLAYER.isTeam(cid) and FLAG.PLAYER.getCaptureFlag(cid) then
local flag = FLAG.PLAYER.getFlagID(cid)
FLAG.TEAM.doCreateFlag(flag)
FLAG.PLAYER.doExit(cid)
return true
end

if FLAG.PLAYER.isTeam(cid) then
FLAG.PLAYER.doExit(cid)
return true
end

return true
end

function onCombat(cid, target)
if FLAG.PLAYER.isTeam(cid) and FLAG.PLAYER.isTeam(target) and FLAG.PLAYER.getTeam(cid) == FLAG.PLAYER.getTeam(target) then
return false
end

	return true
end

function onAttack(cid, target)
if FLAG.PLAYER.isTeam(cid) and FLAG.PLAYER.isTeam(target) and FLAG.PLAYER.getTeam(cid) == FLAG.PLAYER.getTeam(target) then
return false
end

	return true
end
