function onStepIn(cid, item, position, lastPosition, fromPosition, toPosition, actor)
local tile_flag = getItemAttribute(item.uid, "flag")
local team_flag = FLAG.PLAYER.getFlagID(cid)
local team_ = FLAG.PLAYER.getTeamPositionByTeam(FLAG.PLAYER.getTeam(cid))

if FLAG.PLAYER.getTeam(cid) == tile_flag then
doTeleportThing(cid, lastPosition)
return false
end

if not FLAG.PLAYER.getCaptureFlag(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "E necessario a flag para pontuar")
doTeleportThing(cid, lastPosition)
return false
end

if getPlayerStorageValue(cid, 87876) ~= -1 then
doSetPlayerSpeed(cid, getPlayerStorageValue(cid, 87876))
setGlobalStorageValue(cid, 87876, -1)
end

FLAG.addPoint(team_)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Parabéns você conseguiu pountuar")
doBroadcastMessage("O time "..team_.." pontuou")
doBroadcastMessage("Time 1 com "..FLAG.getPointTeam(1).." pontos / Time 2 com "..FLAG.getPointTeam(2).." pontos")
FLAG.PLAYER.setCaptureFlagPlayer(cid, true)
FLAG.TEAM.doCreateFlag(team_flag)
FLAG.PLAYER.doBackPlayerToBase(cid)

if FLAG.getPointTeam(team_) >= configs_event[FLAG.getFlag()].point then
doBroadcastMessage("O time "..team_.." ganhou o Capture The FLAG.")
FLAG.doDeclareWinner(FLAG.PLAYER.getTeam(cid))
return true
end

return false
end
