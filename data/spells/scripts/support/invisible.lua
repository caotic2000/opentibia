local combat = createCombatObject()
setCombatParam(combat, COMBAT_PARAM_EFFECT, CONST_ME_MAGIC_RED)
setCombatParam(combat, COMBAT_PARAM_AGGRESSIVE, false)

local condition = createConditionObject(CONDITION_INVISIBLE)
setConditionParam(condition, CONDITION_PARAM_TICKS, 200000)
setCombatCondition(combat, condition)

function onCastSpell(cid, var)
 --- ADD IN YOUR INVISIBLE ---
  if FLAG.PLAYER.isTeam(cid) then return true end
---ADD IN YOUR INVISIBLE
  
  
	return doCombat(cid, combat, var)
end
