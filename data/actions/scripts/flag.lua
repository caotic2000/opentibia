function effectFlag(cid, effect, team_, color)
if not isPlayer(cid) or not FLAG.PLAYER.getCaptureFlag(cid) then
return true
end

local pos = getCreaturePosition(cid)
doSendMagicEffect(pos, effect)
doSendAnimatedText(pos, "Time "..team_.."", color)

addEvent(effectFlag, 1000, cid, effect, team_, color)
return true
end

function onUse(cid, item, fromPosition, itemEx, toPosition)
local flag = getItemAttribute(item.uid, "flag")
local kill = getItemAttribute(item.uid, "kill")
local function positive(number) return number < 0 and number*-1 or number end

if flag then

print(positive(getThingPos(item.uid).x-getCreaturePosition(cid).x))
print(positive(getThingPos(item.uid).y-getCreaturePosition(cid).y))
if positive(getThingPos(item.uid).x-getCreaturePosition(cid).x) > 2 or positive(getThingPos(item.uid).y-getCreaturePosition(cid).y) > 2 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Você esta muito longe")
return true
end

local team_ = FLAG.PLAYER.getTeamPositionByTeam(FLAG.PLAYER.getTeam(cid))
local flag_ = configs_event[FLAG.getFlag()]

if kill == 1 and flag == FLAG.PLAYER.getTeam(cid) then
FLAG.TEAM.doReturnFlag(item, FLAG.PLAYER.getTeam(cid))
return true
end

if flag == FLAG.PLAYER.getTeam(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "A sua bandeira foi robouda "..FLAG.getPointTeam(team_ == 1 and 2 or 1).." ocasiões")
return true
end

FLAG.PLAYER.setCaptureFlagPlayer(cid, false, flag)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Bandeira roubado")
setPlayerStorageValue(cid, 87876, getCreatureSpeed(cid))
doSetPlayerSpeed(cid, flag_.speed)
doRemoveItem(item.uid)
addEvent(effectFlag, 1000, cid, team[FLAG.PLAYER.getTeam(cid)].effect, team_, team[FLAG.PLAYER.getTeam(cid)].color)
end

return true
end