effect_arrow = true -- EFEITO de flecha na tile que deve se colocar a flag? true or false
MULTI_CLIENT = 10 --- QUANTOS mcs (multiclients) são permitidos para entrar no evento
query_db = true --- Se no seu otserv tiver a query que adiciona a pontuação final marcar true
gifts = {2323} --- Ids dos presentes que o player irá ganhar se ele for o player "Impar" 


event = {
["Sexta"] = { --- Dia da semana em pt-br
["19:52"] = 1  -- Hora do dia / ID da configuração que você vai usar nesse evento em configs_event pode ser qualquer id existente(!)
},

["Domingo"] = {
["16:27"] = 1
}

}

configs_event = { -- Instruções em .txt
[1] = {tp = {x = 1020, y = 1015, z = 0}, team_1 = 1, team_2 = 2, duration = 20, room = {x = 1022, y = 1027, z = 0}, time = 1, speed = 300, point = 5, min_players = 2, alert_time_msg = {5, 2, 1, 0.5}}

}



team = { -- Instruções em .txt(!)
[1] = {cor = "Vermelho", outfit = {lookType = 136, lookHead = 0, lookBody = 132, lookLegs = 113, lookFeet = 94}, id = 1435, base = {x = 1007, y = 1022, z = 0}, flag_pos = {x = 1002, y = 1021, z = 1}, effect = 28, color = 72, tile = 424, pos = {x = 1003, y = 1012, z = 1}},
[2] = {cor = "Verde", outfit = {lookType = 137, lookHead = 0, lookBody = 121, lookLegs = 101, lookFeet = 101}, id = 1436, base = {x = 1034, y = 1023, z = 0}, flag_pos = {x = 1035, y = 1022, z = 1}, effect = 29, color = 60, tile = 424, pos = {x = 1035, y = 1012, z = 1}}

}

itens = { --- Itens que o player irá ganhar se vencer seguido pela quantidade deles
[8924] = 2,
[8926] = 1

}
