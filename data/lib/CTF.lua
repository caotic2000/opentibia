days_weeks = {
["Sunday"] = "Domingo",
["Monday"] = "Segunda",
["Tuesday"] = "Terça",
["Wednesday"] = "Quarta",
["Thursday"] = "Quinta",
["Friday"] = "Sexta",
["Saturday"] = "Sabado"

}


time = {
week = function() return days_weeks[os.date("%A")] end,
hour = function() return os.date("%H")..":"..os.date("%M") end

}

local str,str_2 = 292924, 2929 -- storages globais que acessam a tab quando necessario favor não modificar


function doSetItemUniqueID(item, value, x)
return doItemSetAttribute(item, not x and "uid" or "aid", value)
end

function doBroadcastMessageFlag(msg)
if FLAG.getEvent() then
doBroadcastMessage(msg)
end
return true
end

function doSendBrodInTime(msg, time_, time_resting)
local msg = string.gsub(msg, "y", (time_resting/(1000*60)) >= 1 and (time_resting/(1000*60)) or (time_resting/(1000)))

if time_-time_resting < 0 then
return true
end
addEvent(doBroadcastMessageFlag, time_-time_resting, msg)
return true
end

function getTeleport(pos)
local tile = getTileInfo(pos).teleport
if not tile then
return false
end

for i=1, 255 do
local thing = getThingFromPos({x = pos.x, y = pos.y, z = pos.z, stackpos = i})
if thing.itemid == 1387 then
return true,thing
end
end

return false
end


function doSetPlayerSpeed(cid, speed)
doChangeSpeed(cid, -getCreatureSpeed(cid))
doChangeSpeed(cid, speed)
return true
end

function createTeleportFlag(pos)
local uid = 11232

local _,thing = getTeleport(pos)
if _ then
doSetItemUniqueID(thing.uid, uid)
return true
end

local item = getThing(doCreateItem(1387, pos))

if item.itemid == 0 then
print("ERROR FLAG SYSTEM\nA posição informada do teleport esta errada\nAbortado")
return true
end

doSetItemUniqueID(item.uid, uid)
return item
end

function cleanStorages()
local storages = {13323,13324, 102102, 102103}
for i=1, #storages do
setGlobalStorageValue(storages[i], -1)
end
return true
end

FLAG = {
start = function (flag)
local alert_time_msg = configs_event[flag].alert_time_msg
doBroadcastMessage("O evento de captura de bandeira ja começou")
createTeleportFlag(configs_event[flag].tp)
setGlobalStorageValue(13323, flag)
setGlobalStorageValue(13324, 1)
addEvent(FLAG.doInitEvent, configs_event[FLAG.getFlag()].time*1000*60)

for i,x in pairs(alert_time_msg) do
if x >= 1 then
doSendBrodInTime("Falta y minutos para o evento começar", (configs_event[FLAG.getFlag()].time*1000*60), x*1000*60)
else
doSendBrodInTime("Falta y segundos para o evento começar", (configs_event[FLAG.getFlag()].time*1000*60), x*1000*60)
end
end

return true
end,

getEvent = function ()
return getGlobalStorageValue(13324) ~= -1 and true or false
end,

doInitEvent = function ()

local function doSendEffectArrow(flag)
if not FLAG.getEvent() then return true end
local time_1 = team[flag.team_1]
local time_2 = team[flag.team_2]
doSendMagicEffect(time_1.pos, 55)
doSendMagicEffect(time_2.pos, 55)
addEvent(doSendEffectArrow, 10000, flag)
return true
end

local function getSurplusPlayer(players)
local team_1,team_2 = {},{}
local time_1 = configs_event[FLAG.getFlag()].team_1
local time_2 = configs_event[FLAG.getFlag()].team_2
for i=1, #players do 
if FLAG.PLAYER.getTeam(players[i]) == time_1 then
table.insert(team_1, players[i])
end
if FLAG.PLAYER.getTeam(players[i]) == time_2 then
table.insert(team_2, players[i])
end
end

if #team_1 > #team_2 then
local ran = math.random(1, #team_1)
return {ran, team_1[ran]}
end

if #team_2 > #team_1 then
local ran = math.random(1, #team_2)
return {ran, team_2[ran]}
end

return false
end

local x = 0
local player = getPlayersOnline()
local player_surplus = 0
local players = {}
for i=1, #player do
if FLAG.PLAYER.isTeam(player[i]) then
table.insert(players, player[i])
x = x+1
end
end

if x < configs_event[FLAG.getFlag()].min_players then
doBroadcastMessage("Menos de "..configs_event[FLAG.getFlag()].min_players.." players entraram no evento\nEvento não vai acontencer")
FLAG.doCloseEvent()
return true
end

if #players%2 ~= 0 then
local surplus = getSurplusPlayer(players)
player_surplus = surplus[2]
FLAG.PLAYER.doExit(player_surplus)

doPlayerSendTextMessage(player_surplus, MESSAGE_STATUS_CONSOLE_BLUE, "Você foi o player impar do capture the flag e será kitado")
for itens=1, #gifts do
doPlayerAddItem(player_surplus, gifts[itens])
end

table.remove(players, table.find(players, player_surplus))
end

for i=1, #players do
FLAG.PLAYER.enter(players[i])
end
doBroadcastMessage("O evento CAPTURE THE FLAG já irá começar")
FLAG.TEAM.doCreateFlags()
FLAG.TEAM.doCreateTile_Flag()
FLAG.removeTeleportFlag()
if effect_arrow then
doSendEffectArrow(configs_event[FLAG.getFlag()])
end

setGlobalStorageValue(102102, 0)
setGlobalStorageValue(102103, 0)


addEvent(FLAG.doCloseEvent, configs_event[FLAG.getFlag()].duration*1000*60)

for i,x in pairs(configs_event[FLAG.getFlag()].alert_time_msg) do
if x >= 1 then
doSendBrodInTime("Falta y minutos para o evento acabar", (configs_event[FLAG.getFlag()].duration*1000*60), x*1000*60)
else
doSendBrodInTime("Falta y segundos para o evento acabar", (configs_event[FLAG.getFlag()].duration*1000*60), x*1000*60)
end
end

return true
end,

getFlag = function () return getGlobalStorageValue(13323)  end,

doCloseEvent = function (x)
if getGlobalStorageValue(912221) == 1 then
setGlobalStorageValue(912221, -1)
return true
end 

local flag = configs_event[FLAG.getFlag()]
local player = getPlayersOnline() 
local cid = 1

if not x and FLAG.getPointTeam(1) ~= FLAG.getPointTeam(2) then
local team_ = FLAG.getPointTeam(1) > FLAG.getPointTeam(2) and configs_event[FLAG.getFlag()].team_1 or configs_event[FLAG.getFlag()].team_2
FLAG.doDeclareWinner(team_, true)
doBroadcastMessage("O time "..team_.." ganhou o Capture The FLAG.")
end

FLAG.TEAM.doRemoveFlags()

for i=1, #player do
cid = player[i]
if FLAG.PLAYER.isTeam(cid) then
FLAG.PLAYER.doExit(cid)
end
end
doBroadcastMessage("O evento terminou")
cleanStorages()
return true
end,

getPointTeam = function (team)
return team == 1 and getGlobalStorageValue(102102) or getGlobalStorageValue(102103)
end,

addPoint = function(team)
if team == 1 then
setGlobalStorageValue(102102, getGlobalStorageValue(102102)+1)
return true
end
setGlobalStorageValue(102103, getGlobalStorageValue(102103)+1)
return true
end,

doDeclareWinner = function (team_, t)
local player = getPlayersOnline()
local cid = 0
for i=1, #player do
cid = player[i]
if FLAG.PLAYER.getTeam(cid) == team_ then
for i,x in pairs(itens) do
doPlayerAddItem(cid, i, x)
end
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Parabéns seu time venceu e conseguiu premios")
end
end

local flag = configs_event[FLAG.getFlag()]
local team_1,team_2 = team[flag.team_1], team[flag.team_2]
db.executeQuery("INSERT INTO `flag` VALUES ('".. team_1.cor .."', '".. team_2.cor .."', ".. FLAG.getPointTeam(1) ..","..FLAG.getPointTeam(2)..", '"..team[team_].cor.."')")
if not t then FLAG.doCloseEvent(true) setGlobalStorageValue(912221, 1) return true end
return true
end,

removeTeleportFlag = function ()
local flag = configs_event[FLAG.getFlag()]
local y,tp = getTeleport(flag.tp) 
if y then
doRemoveThing(tp.uid)
end
return true
end,

PLAYER = {
goPlayerEvent = function (cid)

local function getItensName()
local str = ""
for i,x in pairs(itens) do
str = str..getItemInfo(i).name.." quantidade do item "..x.."\n"
end
return str
end

if FLAG.PLAYER.isTeam(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Você ja esta dentro do evento")
return false
end

local teamfree = FLAG.TEAM.getTeamFree()
doShowTextDialog(cid, "1949", "---Regras---\nCapture a flag inimiga e coloque na base do inimigo indicada\n\nPremios:\n"..getItensName())
setPlayerStorageValue(cid, 29192, teamfree)
doTeleportThing(cid, configs_event[FLAG.getFlag()].room)
return true
end,

enter = function (cid) 
local teamfree = FLAG.PLAYER.getTeam(cid)
local t = ""
local out = getCreatureOutfit(cid)
local duration = configs_event[FLAG.getFlag()].duration

doSetCreatureOutfit(cid, team[teamfree].outfit, duration*60*1000)
doTeleportThing(cid, team[teamfree].base)
doCreatureAddHealth(cid, getCreatureMaxHealth(cid))
doCreatureAddMana(cid, getCreatureMaxMana(cid))
return true
end,

doRetireFlagPlayer = function (cid)
local flag = FLAG.PLAYER.getFlagID(cid)

if getPlayerStorageValue(cid, 87876) ~= -1 then
doSetPlayerSpeed(cid, getPlayerStorageValue(cid, 87876))
setGlobalStorageValue(cid, 87876, -1)
end

FLAG.PLAYER.setCaptureFlagPlayer(cid, true)
FLAG.TEAM.doCreateKillFlag(getPlayerPosition(cid), flag)
doCreatureAddHealth(cid, getCreatureMaxHealth(cid))
doCreatureAddMana(cid, getCreatureMaxMana(cid))
doTeleportThing(cid, team[FLAG.PLAYER.getTeam(cid)].base)
return true
end,

doBackPlayerToBase = function (cid)

if getPlayerStorageValue(cid, 87876) ~= -1 then
doSetPlayerSpeed(cid, getPlayerStorageValue(cid, 87876))
setGlobalStorageValue(cid, 87876, -1)
end

doCreatureAddHealth(cid, getCreatureMaxHealth(cid))
doCreatureAddMana(cid, getCreatureMaxMana(cid))
doTeleportThing(cid, team[FLAG.PLAYER.getTeam(cid)].base)
return true
end,

isTeam = function (cid)
return getPlayerStorageValue(cid, 29192) ~= -1 and true or false
end,

getTeam = function (cid)
return getPlayerStorageValue(cid, 29192)
end,

getTeamPositionByTeam = function (team)
return configs_event[FLAG.getFlag()].team_1 == team and 1 or 2
end,

setNullTeam = function (cid)
setPlayerStorageValue(cid, 29192, -1)
return true
end,

getCaptureFlag = function (cid)
return getPlayerStorageValue(cid, 28282) ~= -1 and true or false
end,

getFlagID = function (cid)
return getPlayerStorageValue(cid, 28282)
end,

setCaptureFlagPlayer = function(cid, x, flag)
setPlayerStorageValue(cid, 28282, x == true and -1 or flag)
return true
end,

doExit = function (cid) 

if getPlayerStorageValue(cid, 87876) ~= -1 then
doSetPlayerSpeed(cid, getPlayerStorageValue(cid, 87876))
setGlobalStorageValue(cid, 87876, -1)
end

doTeleportThing(cid, getTownTemplePosition(getPlayerTown(cid)))
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Você terminou o the flag")
FLAG.PLAYER.setNullTeam(cid)
FLAG.PLAYER.setCaptureFlagPlayer(cid, true)
doCreatureAddHealth(cid, getCreatureMaxHealth(cid))
doCreatureAddMana(cid, getCreatureMaxMana(cid))
return true
end,

},

TEAM = {
members = function (team)
local player = getPlayersOnline()
local teams = {}
for i=1, #player do
if getPlayerStorageValue(player[i], 29192) == team then
table.insert(teams, player[i])
end
end
return teams 
end,

doCreateFlags = function ()
local flag = configs_event[FLAG.getFlag()]
local item = doCreateItem(team[flag.team_1].id, team[flag.team_1].flag_pos)
local item_2 = doCreateItem(team[flag.team_2].id, team[flag.team_2].flag_pos)

doSetItemUniqueID(item, "12922", true)
doSetItemUniqueID(item_2, "12922", true)
doItemSetAttribute(item, "flag", flag.team_1)
doItemSetAttribute(item_2, "flag", flag.team_2)
return true
end,

doCreateFlag = function (flag)
local item = doCreateItem(team[flag].id, team[flag].flag_pos)

doSetItemUniqueID(item, "12922", true)
doItemSetAttribute(item, "flag", flag)
return true
end,

doCreateKillFlag = function (pos, flag)

local function setTimeHasEvent(pos)
local timer = 30
if not FLAG.getEvent() then
for i=1, 255 do
local thing = getThingFromPos({x = pos.x, y = pos.y, z = pos.z, stackpos = i})
if thing and thing.itemid ~= 0 and getItemAttribute(thing.uid, "flag") then
doRemoveItem(thing.uid)
end
end
return true
end

addEvent(setTimeHasEvent, 1000*timer, pos)
return true
end


local item = doCreateItem(team[flag].id, {x = pos.x, y = pos.y, z = pos.z, stackpos = 255})

doSetItemUniqueID(item, "12922", true)
doItemSetAttribute(item, "flag", flag)
doItemSetAttribute(item, "kill", 1)
setTimeHasEvent(pos)
return true
end,

doReturnFlag = function (item, flag)
FLAG.TEAM.doCreateFlag(flag)
doRemoveItem(item.uid)
return true
end,

doRemoveFlags = function ()
local flag = configs_event[FLAG.getFlag()]
local time_1 = team[flag.team_1]
local time_2 = team[flag.team_2]

local x = {time_1, time_2}
for t=1, #x do
for i=1, 255 do
local thing = getThingFromPos({x = x[t].flag_pos.x, y = x[t].flag_pos.y, z = x[t].flag_pos.z, stackpos = i})
if thing and thing.itemid ~= 0 and getItemAttribute(thing.uid, "flag") then
doRemoveItem(thing.uid)
end
end
end


return true
end,

getTeams = function () return {configs_event[FLAG.getFlag()].team_1, configs_event[FLAG.getFlag()].team_2} end,

doCreateTile_Flag = function ()
local flag = FLAG.getFlag()
local team_1 = configs_event[flag].team_1
local team_2 = configs_event[flag].team_2

local tile = getThingFromPos({x = team[team_1].pos.x, y = team[team_1].pos.y, z = team[team_1].pos.z, stackpos = 0})
local tile_ = getThingFromPos({x = team[team_2].pos.x, y = team[team_2].pos.y, z = team[team_2].pos.z, stackpos = 0})

doSetItemUniqueID(tile.uid, 67212, true)
doSetItemUniqueID(tile_.uid, 67212, true)
doItemSetAttribute(tile.uid, "team", 1)
doItemSetAttribute(tile_.uid, "team", 2)
doItemSetAttribute(tile.uid, "flag", team_1)
doItemSetAttribute(tile_.uid, "flag", team_2)
return true
end,

getTeamFree = function() 
local function random(x, y)

if math.random(1,2) == 1 then 
return x
end
return y
end

local x = {FLAG.TEAM.members(FLAG.TEAM.getTeams()[1]), FLAG.TEAM.members(FLAG.TEAM.getTeams()[2])}
return #x[1] > #x[2] and FLAG.TEAM.getTeams()[2] or #x[1] == #x[2] and random(FLAG.TEAM.getTeams()[1], FLAG.TEAM.getTeams()[2]) or FLAG.TEAM.getTeams()[1]
end

}

}

